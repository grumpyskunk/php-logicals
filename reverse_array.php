<?php

include 'lib/common.php';

/**
 * Reverse an array in PHP without utilizing the array_reverse method
 */

$index_array = [
  1,
  2,
  3,
  4,
  5,
];

reverse($index_array);

print_r($index_array);

$assoc_array = [
  'a' => 1,
  'b' => 2,
  'c' => 3,
  'd' => 4,
  'e' => 5,
];

reverse($assoc_array);

print_r($assoc_array);

function reverse(array &$array): void {
  // If this is an associative array
  if (isAssociativeArray($array)) {
    // Pluck and call self to reverse the key sequence
    $keys = array_keys($array);
    reverse($keys);

    // Create an associative array of keys => keys
    $keys = array_combine($keys, $keys);

    // Merge the new key order with the original array to remap the values
    $array = array_merge($keys, $array);

    return;
  }

  // For non-associative arrays, initialize our starting index and its opposite (last) index
  $index     = 0;
  $opp_index = count($array) - 1;

  do {
    // Get the value for our current position
    $current = $array[$index];
    // Get the value for our opposite position
    $opposite = $array[$opp_index];

    // Reassign our values
    $array[$index]     = $opposite;
    $array[$opp_index] = $current;

    // Tip the scales
    $index++;
    $opp_index--;
    // While we have not reached/passed the middle value
  } while ($index < $opp_index);

}
