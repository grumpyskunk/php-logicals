<?php

include 'lib/common.php';

/**
 * write a program that prints the numbers from 1 to 100.
 * But for multiples of three print “Fizz” instead of the number and for
 * the multiples of five print “Buzz”.
 *
 * For numbers which are multiples of both three and five print “Fizz Buzz”
 */

$fizz_buzz = 15;
$fizz      = 3;
$f_string  = 'Fizz';
$buzz      = 5;
$b_string  = 'Buzz';

for ($x = 1; $x <= 100; $x++) {
  switch (true) {
    case isDivisible($x, $fizz_buzz):
      $out = $f_string . ' ' . $b_string;
      break;
    case isDivisible($x, $fizz):
      $out = $f_string;
      break;
    case isDivisible($x, $buzz):
      $out = $b_string;
      break;
    default:
      $out = $x;
  }

  put($out);
}