<?php

include 'lib/common.php';

/**
 * Determine if a string, with all non alpha-numeric characters removed,
 * is a palindrome.
 */

$string        = 'A man, a plan, a canal, Panama!';
$is_palindrome = is_palindrome($string) ? 'Yes' : 'No';
put($string . ' is a palindrome: ' . $is_palindrome);

$string        = 'RaceC@r!';
$is_palindrome = is_palindrome($string) ? 'Yes' : 'No';
put($string . ' is a palindrome: ' . $is_palindrome);

$string        = 'RaceCar!';
$is_palindrome = is_palindrome($string) ? 'Yes' : 'No';
put($string . ' is a palindrome: ' . $is_palindrome);

function is_palindrome(string $input): bool {
  $stripped = preg_replace('/[^a-z0-9]/', '', strtolower($input));

  return $stripped === strrev($stripped);
}
