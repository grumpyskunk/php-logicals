<?php

include 'lib/common.php';

/**
 * Recursively operating off of the following tree, evaluate the sum
 * of all nodes and their related children.
 *
 * https://en.wikipedia.org/wiki/Tree_(data_structure)
 */

$node_1        = new stdClass();
$node_1->value = 5;

$node_2        = new stdClass();
$node_2->value = 13;

$node_3 = new stdClass();

$node_4        = new stdClass();
$node_4->value = 7;

$node_5        = new stdClass();
$node_5->value = 21;

$node_6        = new stdClass();
$node_6->value = 0;

$node_7        = new stdClass();
$node_7->value = 11;

$node_1->children = [$node_2, $node_4];
$node_2->children = [$node_5];
$node_3->children = [$node_6, $node_7];
$node_5->children = [$node_3];

put(sum($node_1));

function sum($node): int {
  // Initialize our sum value for this node's value, if it has one
  $sum = $node->value ?? 0;

  // Iterate any applicable child nodes to recursively add their sum
  foreach ($node->children ?? [] as $child) {
    $sum += sum($child);
  }

  return $sum;
}