<?php

/**
 * Validates if an array is an associative array
 *
 * @param array $to_test
 *
 * @return bool
 */
function isAssociativeArray(array $to_test): bool {
  $size = count($to_test);

  if ($size > 0) {
    // Create an array of integers for the size of our input array minus one for zero
    $expected = range(0, $size - 1);

    if (array_keys($to_test) !== $expected) {
      return true;
    }
  }

  return false;
}

/**
 * Evaluates if the numerator is evenly divisible by the denominator
 *
 * @param int $numerator
 * @param int $denominator
 *
 * @return bool
 */
function isDivisible(int $numerator, int $denominator): bool {
  return $numerator % $denominator === 0;
}

function put($input): void {
  $type = gettype($input);

  if ($type !== 'array' && $type !== 'object') {
    print_r($input . PHP_EOL);
  } else {
    print_r(print_r($input, true));
  }
}

/**
 * Checks if the given string starts with the specified substring
 *
 * @param string $haystack    String to check against
 * @param string $starts_with String to check if starts with
 *
 * @return bool
 */
function startsWith(string $haystack, string $starts_with): bool {
  return strpos($haystack, $starts_with) === 0;
}
